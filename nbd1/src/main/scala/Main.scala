import scala.annotation.tailrec

object Main {
  def listToStr1(list: List[String]): String = {
    var res = ""
    for (element <- list) {
      res += element + ", "
    }
    res.substring(0, res.length() - 2)
  }

  def listToStr2(list: List[String]): String = {
    var res = ""
    for (element <- list) {
      if (element.startsWith("S")) res += element + ", "
    }
    if (res.nonEmpty) res.substring(0, res.length() - 2) else ""
  }

  def listToStr3(list: List[String]): String = {
    var res = ""
    var i = 0
    while (i < list.length) {
      res += list(i) + ", "
      i = i + 1
    }
    if (res.nonEmpty) res.substring(0, res.length() - 2) else ""
  }

  def listToStrRecursive1(list: List[String]): String = {
    if (list.tail.nonEmpty) list.head + ", " + listToStrRecursive1(list.tail) else list.head
  }

  def listToStrRecursive2(list: List[String]): String = {
    if (list.tail.nonEmpty) listToStrRecursive2(list.tail) + ", " + list.head else list.head
  }

  @tailrec def listToStrTailRecursion1(str: String, list: List[String]): String = {
    if (list.tail.nonEmpty) listToStrTailRecursion1(str + list.head + ", ", list.tail)
    else str + list.head + ", "
  }

  def listToStrFoldl(list: List[String]): String = {
    list.foldLeft("")((a, b) => a + b + ", ")
  }

  def listToStrFoldr(list: List[String]): String = {
    list.foldRight("")((a, b) => a + ", " + b)
  }

  def listToStrFoldlStarsWithS(list: List[String]): String = {
    list.filter(d => d.startsWith("S")).foldLeft("")(_ + _ + ", ")
  }

  def increaseListElements(list: List[Int]): List[Int] = list map (l => l + 1)

  def absolutesOfElementsInRange(list: List[Int], rangeStart: Int, rangeEnd: Int): List[Int] = {
    list.filter(n => n <= rangeEnd && n >= rangeStart) map (l => Math.abs(l))
  }

  def printTupleElements(tuple: (String, Int, Double)): Unit = {
    println("%s, %s, %f".format(tuple._1, tuple._2, tuple._3))
  }

  @tailrec def removeZerosFromList(list: List[Int], res: List[Int] = List.empty): List[Int] = {
    list match {
      case 0 :: t => removeZerosFromList(t, res)
      case h :: t => removeZerosFromList(t, h :: res)
      case Nil => res.reverse
    }
  }

  object Person {
    private val people = Map("Andrzej" -> 11, "Bogdan" -> 25)
    def getPersonAge(name: String): Option[Int] = people.get(name)
  }

  class Product(name : String, price : Double) {
    private var _producer : Option[String] = None

    def setProducer(producer : String) : Unit = {
      _producer = Option(producer)
    }

    override def toString: String = {
      "Amazing " + name + " for only $" + price + " produced by " + _producer.getOrElse("unknown brand")
    }
  }

  def main(args: Array[String]): Unit = {
    val days = List("Monday", "Tuesday", "Wednesday", "Thursday", "Saturday", "Sunday")
    println(days)
    println(listToStr1(days))
    println(listToStr2(days))
    println(listToStr3(days))
    println(listToStrRecursive1(days))
    println(listToStrRecursive2(days))
    println(listToStrTailRecursion1("", days))
    println(listToStrFoldl(days))
    println(listToStrFoldr(days))

    val pricing = Map(
      "Pizza with Salami" -> 5.0,
      "Pizza with Pineapple (Exclusive)" -> 10.0,
      "Pizza with Tomatoes" -> 4.0,
    )
    val discountedPricing = pricing map { case (kind, price) => (kind, price * 0.9) }
    println(discountedPricing)

    println(increaseListElements(List(1, 2, 3, 4)))
    println(absolutesOfElementsInRange(List(-1, 2, -3, 4), -5, 12))
    printTupleElements(("string", 123, 21.37))
    println(removeZerosFromList(List(0, 1, 2, 3, 4)))
    println(Person.getPersonAge("Andrzej"))
    println(Person.getPersonAge("Ryszard"))

    val coat = new Product("Coat", 15)
    val shoes = new Product("Shoes", 6)
    val gloves = new Product("Gloves", 1)
    shoes.setProducer("Pumba")
    coat.setProducer(null)
    println(coat)
    println(shoes)
    println(gloves)
  }
}
